package com.example.basis

import com.example.basis.Model.Main
import com.example.basis.Retrofit.Api
import io.reactivex.Observable


class CardRepository(private val apiService:Api) {

     fun fetchData() : Observable<List<Main>> {
         return apiService.getData()
     }

}