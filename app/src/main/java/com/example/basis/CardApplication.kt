package com.example.basis

import android.app.Application
import com.example.basis.Retrofit.Api

  class CardApplication : Application() {

    private val apiService: Api = Api.create()
     val cardRepository =  CardRepository(apiService)
}