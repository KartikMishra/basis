package com.example.basis

import com.airbnb.mvrx.BaseMvRxViewModel
import com.airbnb.mvrx.MvRxState

abstract class CardViewModel<S: MvRxState>(state: S):
        BaseMvRxViewModel<S>(state , debugMode = BuildConfig.DEBUG)