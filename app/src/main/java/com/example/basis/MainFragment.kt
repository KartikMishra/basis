package com.example.basis

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.airbnb.mvrx.*
import com.example.basis.Model.Main
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


data class CardState(val postList: Async<List<Main>> = Uninitialized) : MvRxState

class MvRxCardViewModel(state: CardState, cardRepository: CardRepository) : CardViewModel<CardState>(state) {

    init {
           cardRepository.fetchData()
                         .observeOn(AndroidSchedulers.mainThread())
                         .subscribeOn(Schedulers.io())
                         .execute { copy( postList = it) }
        }


      companion object : MvRxViewModelFactory<MvRxCardViewModel, CardState> {
          override fun create(viewModelContext: ViewModelContext, state: CardState): MvRxCardViewModel? {
              val cardRepository = viewModelContext.app<CardApplication>().cardRepository
              return MvRxCardViewModel(state, cardRepository)
          }
      }
}

class MainFragment : BaseMvRxFragment() {

    override fun invalidate() = withState(viewModel) { state ->

        if (state.postList.complete) {
            onDataRecieved(state.postList)
        }

    }

    private fun onDataRecieved(postList: Async<List<Main>>) {

        mAdapter = ViewPagerAdapter(postList(), activity)
        mViewPager?.adapter = mAdapter

    }

    val viewModel: MvRxCardViewModel by fragmentViewModel()

    private var mViewPager: ViewPager? = null
    private var mAdapter: ViewPagerAdapter? = null

    private var mContents: List<Main>? = null

    private var previous: TextView? = null
    private var next:TextView? = null
    private var reset: ImageView? = null




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view : View = inflater.inflate(R.layout.fragment_main, container, false)


        mViewPager = view.findViewById(R.id.viewPager)
        mContents = ArrayList()
        previous = view.findViewById(R.id.previousBtn)
        next = view.findViewById(R.id.nextBtn)
        reset = view.findViewById(R.id.reset)

        mViewPager?.setPageTransformer(true, ViewPagerStack())
        mViewPager?.offscreenPageLimit = 10
        return view
    }

    private inner class ViewPagerStack : ViewPager.PageTransformer {

        override fun transformPage(page: View, position: Float) {
            if (position >= 0) {
                page.scaleX = 0.9f - 0.002f * position
                page.scaleY = 0.8f

                page.translationX = -page.width * position
                page.translationY = 0.4f * position


                if (mViewPager?.currentItem!! > 0) {
                    previous?.visibility = View.VISIBLE
                } else if (mViewPager?.currentItem == 0) {
                    previous?.visibility = View.GONE
                }

                previous?.setOnClickListener { mViewPager?.arrowScroll(View.FOCUS_LEFT) }
                next?.setOnClickListener{ mViewPager?.arrowScroll(View.FOCUS_RIGHT) }


                reset?.setOnClickListener{
                    mViewPager?.setCurrentItem(0, true)
                }

            }
        }
    }

}