package com.example.basis;

import android.os.Bundle;

import com.airbnb.mvrx.BaseMvRxActivity;


public class MainActivity extends BaseMvRxActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
